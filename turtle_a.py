from collections import namedtuple, deque
from functools import partial
from turtle import *

import random
import sys
import tkinter

from bezier import BezierCurve
import ttfparser


font_size = 144 # in px

xorigin = lambda: -getcanvas().winfo_width() / 2 + 10
xoffset = xorigin()
drawing = False
queue = deque(maxlen=16)


def scale_to_fontsize(fontinfo, x, y=None):
    if y is None:
        ret = x / fontinfo.units_per_em * font_size
    else:
        ret = (x / fontinfo.units_per_em * font_size,
               y / fontinfo.units_per_em * font_size)        

    return ret


def draw_rect(x1, y1, x2, y2):
    penup()
    goto(x1 + xoffset, y1)
    pendown()
    goto(x1 + xoffset, y2)
    goto(x2 + xoffset, y2)
    goto(x2 + xoffset, y1)
    goto(x1 + xoffset, y1)
    penup()


def draw_max_bbox(fontinfo):
    x1scaled, y1scaled = scale_to_fontsize(fontinfo, fontinfo.xmin, fontinfo.ymin)
    x2scaled, y2scaled = scale_to_fontsize(fontinfo, fontinfo.xmax, fontinfo.ymax)
    r = random.randint(5, 15)
    y1scaled += r
    y2scaled += r

    pen(speed=10, pencolor="blue")
    draw_rect(x1scaled, y1scaled, x2scaled, y2scaled)


def draw_glyph_bbox(fontinfo, glyph):
    x1, y1 = glyph.bbox[0]
    x2, y2 = glyph.bbox[1]

    x1scaled, y1scaled = scale_to_fontsize(fontinfo, x1, y1) # x1 - glyph.lsb, y1)
    x2scaled, y2scaled = scale_to_fontsize(fontinfo, x2, y2) # x2 - glyph.lsb, y2)

    pen(speed=10, pencolor="red")
    draw_rect(x1scaled, y1scaled, x2scaled, y2scaled)


def draw_glyph(font_parser, glyph):
    global xoffset, queue, drawing

    if drawing:
        queue.append(glyph)
        return

    drawing = True
    print("Drawing", glyph, "\n")
    
    fontinfo = font_parser.font_info

    draw_max_bbox(fontinfo)
    draw_glyph_bbox(fontinfo, glyph)

    pen(pencolor="black", speed=10)

    for points in glyph.contours:
        penup()
        for x, y in BezierCurve(points).interpolate(intermediate=2):
            xscaled, yscaled = scale_to_fontsize(fontinfo, x, y) # x - glyph.lsb, y)
            goto(xscaled + xoffset, yscaled)
            pendown()

    penup()
    xoffset += scale_to_fontsize(fontinfo, glyph.awidth)
    drawing = False

    if queue:
        draw_glyph(font_parser, queue.popleft())


def draw_guideline():    
    x = xorigin()
    pen(speed=10, pencolor="green")
    goto(x, 0)
    pendown()
    goto(getcanvas().winfo_width(), 0)
    penup()
    goto(x, 0)


def reset_screen():
    global xoffset

    xoffset = xorigin()
    pen(speed=10, pencolor="black")
    penup()
    goto(xoffset, 0)
    clear()
    draw_guideline()


def bind_events(font_parser, glyphs):
    for glyph_name, glyph in glyphs.items():
        if glyph.keycode is None or glyph_name.startswith("uni"):
            continue

        replaces = {
            "one": "1",
            "two": "2",
            "three": "3",
            "four": "4",
            "five": "5",
            "six": "6",
            "seven": "7",
            "eight": "8",
            "nine": "9",
            "zero": "0",
        }

        glyph_name = replaces.get(glyph_name, glyph_name)
        f = partial(draw_glyph, font_parser, glyph)
 
        try:
            onkeypress(f, glyph_name)
        except tkinter.TclError:
            print(f"'{glyph_name}' not bindable")

    onkeypress(reset_screen, "BackSpace")
    listen()


def main():
    try:
        ttf_file = sys.argv[1]
    except IndexError:
        ttf_file = "monspacn.ttf"

    font_parser = ttfparser.TTFParser(ttf_file)
    glyphs = font_parser.parse()
    print("Done parsing ttf")
    print("", font_parser.font_info)
    bind_events(font_parser, glyphs)
    reset_screen()
    ht()

    tkinter.mainloop()


if __name__ == "__main__":
    main()
