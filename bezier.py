
class BezierPoint:
    def __init__(self, x, y, on):
        self.x = x
        self.y = y
        self.on = bool(on)

    def __eq__(self, obj):
        return self.dump() == obj.dump()

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.on

    def __repr__(self):
        return f"{self.__class__.__name__}({self.x}, {self.y}, {self.on})"

    def dump(self):
        return self.x, self.y, self.on

    def mean_point(self, obj):
        new_x = (self.x + obj.x) / 2
        new_y = (self.y + obj.y) / 2

        return BezierPoint(new_x, new_y, True)

    def offset(self, x, y):
        self.x += x
        self.y += y

    def copy(self):
        return BezierPoint(self.x, self.y, self.on)

    is_real = property(lambda self: self.on)
    is_control = property(lambda self: not self.on)


class BezierCurve:
    def __init__(self, points):
        if len(points) < 2:
            raise ValueError("Not enough points supplied (at least 2 needed)")

        self.points = points
        self.numpoints = len(points)

    def __iter__(self):
        return iter(self.points)

    def interpolate(self, intermediate=4):
        if intermediate < 1:
            raise ValueError("Interpolated points must be >= 1")

        increment = 1 / intermediate
        i = 0

        while i < self.numpoints - 1:
            p0 = self.points[i]
            p1 = self.points[i + 1]

            if p0.is_real and p1.is_real:
                # print(f"   Linear Curve from {p0} to {p1}")
                yield p0.x, p0.y
                yield p1.x, p1.y
                i += 1
            elif p0.is_real and p1.is_control:
                try:
                    p2 = self.points[i + 2]
                except IndexError:
                    raise ValueError("Incomplete quadratic Bezier curve supplied (missing p2).")

                if p2.is_control:
                    raise ValueError("Invalid quadratic Bezier curve supplied (p2 is not a real point).")

                # print(f"   Quadratic Curve from {p0} to {p2}, control {p1}")
                t = 0
                while t <= 1:
                    tsquare = t ** 2
                    invtsquare = (1 - t) ** 2
                    twotinvt = 2 * t * (1 - t)
                    x = invtsquare * p0.x + twotinvt * p1.x + tsquare * p2.x
                    y = invtsquare * p0.y + twotinvt * p1.y + tsquare * p2.y
                    t += increment
                    yield x, y                    

                i += 2
            else:
                raise ValueError("Unrecognized Bezier curve supplied.")
