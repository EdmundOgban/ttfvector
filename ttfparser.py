from collections import namedtuple

import fontTools.ttLib.tables._g_l_y_f as _g_l_y_f
from fontTools.ttLib import TTFont

from bezier import BezierPoint


FontInfo = namedtuple("FontInfo", ["xmin", "xmax", "ymin", "ymax", "units_per_em"])
Glyph = namedtuple("Glyph", ["name", "keycode", "bbox", "awidth", "lsb", "contours"])


class TTFParser:
    def __init__(self, ttf_file):
        self.glyphs = dict()
        self.ttf_file = ttf_file
        self.ttfont = TTFont(ttf_file)

    def _parse_composite(self, glyph_name, glyph):
        contours = []

        for component in glyph.components:
            comp_name = component.glyphName

            for contour in self.glyphs[comp_name].contours:
                points = []
                for point in contour:
                    new_pt = point.copy()
                    new_pt.offset(component.x, component.y)
                    points.append(new_pt)

                contours.append(points)

        return contours

    def _parse_contours(self, glyph_name, glyph, close_spline=True):
        contours = []
        pts_start = 0

        for i in range(glyph.numberOfContours):
            points = []
            first_pt = None
            prev_pt = None
            last_pt_idx = glyph.endPtsOfContours[i]

            for pt_idx in range(pts_start, last_pt_idx + 1):
                x, y = glyph.coordinates[pt_idx]
                on = glyph.flags[pt_idx] & _g_l_y_f.flagOnCurve
                pt = BezierPoint(x, y, on)

                if first_pt is None:
                    first_pt = pt

                # Lookbehind missing point interpolation
                if prev_pt is not None:
                    if pt.is_control and prev_pt.is_control:
                        points.append(prev_pt.mean_point(pt))

                points.append(pt)
                prev_pt = pt

            assert first_pt is not None

            if first_pt.is_control:
                first_pt = prev_pt.mean_point(first_pt)
                points.insert(0, first_pt)

            if prev_pt.is_control:
                points.append(first_pt)

            # Close the spline
            if prev_pt.is_real and close_spline:
                points.append(first_pt)

            pts_start = last_pt_idx + 1
            contours.append(points)

        return contours

    def _parse_glyph(self, glyph_name, glyph, keycode, composites, parse_composite=False):
        glyf_table = self.ttfont["glyf"]
        glyph.compile(glyf_table)

        metrics = self.ttfont["hmtx"].metrics
        awidth, lsb = metrics.get(glyph_name, (0, 0))
        contours = []

        try:
            ll = glyph.xMin, glyph.yMin
            ur = glyph.xMax, glyph.yMax
        except AttributeError:
            print(f"Glyph '{glyph_name}' has no bounding box")
            ll = 0, 0
            ur = 0, 0

        self.glyphs[glyph_name] = Glyph(
            name=glyph_name, keycode=keycode, bbox=(ll, ur),
            awidth=awidth, lsb=lsb, contours=contours)

        if glyph.isComposite():
            if parse_composite:
                contours.extend(self._parse_composite(glyph_name, glyph))
            else:
                # Postpone composite glyphs parsing
                composites.append((glyph_name, glyph))
        else:
            contours.extend(self._parse_contours(glyph_name, glyph, close_spline=True))

    def parse(self):
        head_table = self.ttfont["head"]
        cmap_table = self.ttfont["cmap"].getcmap(3, 1)
        glyf_table = self.ttfont["glyf"]

        keymap = {name: key for key, name in cmap_table.cmap.items()}
        composites = []

        self._font_info = FontInfo(
            xmin=head_table.xMin, xmax=head_table.xMax,
            ymin=head_table.yMin, ymax=head_table.yMax,
            units_per_em=head_table.unitsPerEm)

        for glyph_name, glyph in glyf_table.glyphs.items():
            keycode = keymap.get(glyph_name, None)
            self._parse_glyph(
                glyph_name, glyph, keycode, composites)

        for glyph_name, glyph in composites:
            keycode = keymap.get(glyph_name, None)
            self._parse_glyph(
                glyph_name, glyph, keycode, composites, parse_composite=True)

        return self.glyphs

    @property
    def font_info(self):
        if self._font_info is None:
            self.parse()

        return self._font_info
